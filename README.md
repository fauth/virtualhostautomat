# Info

This is the first version of code. There will be others very soon. It'll be improved.
For now, it's only possible to create and deleting a virtual host configuration.

# Creating a new virtual host
    vhost -a

# Removing a virtual host
    vhost -d <hostname>