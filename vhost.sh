#!/bin/bash

#./vhost
# Autor: Diogo Alexsander Cavilha
# Data: 06/05/2014
#
# Descrição:
#   Cria a configuração de virtual host.
#   Gera o link simbólico para /etc/apache2/sites-enabled
#   Adiciona IP e Server name no arquivo /etc/hosts
#   Reinicia o servidor Apache
#
# Opções:
#   -a          Adiciona/Cria um novo host.
#   -d <name>   Deleta um host.

messageRed()
{
    echo -e "\n\033[0;31m$1\033[0m\n"
    sleep 1
}

if [ ! "$USER" == "root" ]; then
    messageRed "Você precisa estar logado como root"
else
    pathHosts="/etc/hosts"
    pathHostsAvailable="/etc/apache2/sites-available"
    pathHostsEnabled="/etc/apache2/sites-enabled"

    messageBlue()
    {
        echo -e "\n\033[0;34m$1\033[0m\n"
        sleep 1
    }

    messageGreen()
    {
        echo -e "\n\033[0;32m$1\033[0m\n"
        sleep 1
    }

    fileExists()
    {
        if [ -e "$1" ]; then
            return 1
        else
            return 0
        fi
    }

    folderExists()
    {
        if [ -d "$1" ]; then
            return 1
        else
            return 0
        fi
    }

    isUsedPort()
    {
        for filename in $(ls /etc/apache2/sites-available)
        do
            port=$(head -1 /etc/apache2/sites-available/"$filename")

            if [ "Listen $1" ==  "$port" ]; then
                return 1
            fi
        done

        return 0
    }

    requireFileName()
    {
        echo -n "Nome do arquivo (arquivo que conterá a configuração): "
        read vhFileName

        $(fileExists "$pathHostsAvailable/$vhFileName")

        if [ $? -eq 1 ]; then
            messageRed "[$pathHostsAvailable/$vhFileName] Arquivo já existe"
            requireFileName
        fi
    }

    requireApplicationFolder()
    {
        echo -n "Diretório da aplicação (para onde o vhost deve apontar): "
        read vhPath

        $(folderExists "$vhPath")

        if [ $? -eq 0 ]; then
            messageRed "[$vhPath] O diretório não existe."
            requireApplicationFolder
        fi
    }

    requireApplicationPort()
    {
        echo -n "Porta: "
        read vhPort

        $(isUsedPort "$vhPort")

        if [ $? -eq 1 ]; then
            messageRed "[$vhPort] Esta porta já está sendo utilizada em outro host"
            requireApplicationPort
        fi
    }

    addHost()
    {
        requireFileName
        requireApplicationFolder

        echo -n "Server name. (Ex: minhaaplicacao.local): "
        read vhServerName

        requireApplicationPort

        messageBlue "Criando host..."
        touch $pathHostsAvailable/$vhFileName

        if [ -z $vhPort ]; then
            `echo "<VirtualHost *:80>" >> $pathHostsAvailable/$vhFileName`
        else
            `echo "Listen $vhPort" >> $pathHostsAvailable/$vhFileName`
            `echo "<VirtualHost *:$vhPort>" >> $pathHostsAvailable/$vhFileName`
        fi

        `echo -e "\tDocumentRoot $vhPath" >> $pathHostsAvailable/$vhFileName`

        if [ ${#vhServerName} -gt 0 ]; then
            `echo -e "\tServerName $vhServerName" >> $pathHostsAvailable/$vhFileName`
            `echo "" >> $pathHostsAvailable/$vhFileName`
            # `echo -e "\n" >> $pathHosts`
            # `echo -e "127.0.0.1\t$vhServerName" >> $pathHosts`
        else
            `echo -e "\t#ServerName $vhFileName.local" >> $pathHostsAvailable/$vhFileName`
        fi

        `echo -e "\t#SetEnv VAR_NAME\t\"value\"" >> $pathHostsAvailable/$vhFileName`
        `echo -e "\t<Directory $vhPath/>" >> $pathHostsAvailable/$vhFileName`
        `echo -e "\t\tOptions Indexes FollowSymLinks MultiViews" >> $pathHostsAvailable/$vhFileName`
        `echo -e "\t\tAllowOverride All" >> $pathHostsAvailable/$vhFileName`
        `echo -e "\t\tOrder allow,deny" >> $pathHostsAvailable/$vhFileName`
        `echo -e "\t\tallow from all" >> $pathHostsAvailable/$vhFileName`
        `echo -e "\t</Directory>" >> $pathHostsAvailable/$vhFileName`
        `echo "</VirtualHost>" >> $pathHostsAvailable/$vhFileName`

        messageBlue "Habilitando host..."
        #ln -s $pathHostsAvailable/$vhFileName /etc/apache2/sites-enabled/
        a2ensite $vhFileName

        if [ ${#vhServerName} -gt 0 ]; then
            messageBlue "Adicionando entrada no arquivo /etc/hosts"
            `echo -e "\n" >> $pathHosts`
            `echo -e "127.0.0.1\t\t$vhServerName" >> $pathHosts`
        fi

        messageBlue "Reiniciando o apache..."
        service apache2 restart

        messageBlue "Host criado com sucesso!"
    }

    removeHost()
    {
        host=$1

        if [ ${#host} -gt 0 ]; then
            if [ -e "$pathHostsEnabled/$host" ]; then
                rm -f "$pathHostsEnabled/$host"
                rm -f "$pathHostsAvailable/$host"
                messageBlue "Host deletado."
            else
                messageBlue "[$pathHostsEnabled/$host] Host não existe."
            fi
        else
            messageRed "Host não informado."
        fi
    }

    case $1 in
        "-a") addHost;;
        "-d") removeHost "$2";;
    esac
fi